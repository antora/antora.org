/* antora.org */

/* TEAM */
Lead: Sarah White
Twitter: @carbonfray
From: Denver, CO

Lead: Dan Allen
Twitter: @mojavelinux
From: Denver, CO

/* SITE */
Language: English
Standards: HTML5, CSS3, JavaScript
Components: Bulma, Font Awesome, Google Fonts, Google Analytics
Software: Linux, Chrome, Firefox, Ruby, Middleman, Asciidoctor, Tilt, Sprockets, Slim, Sass, Git, VIM, Inkscape, Gimp, Rake, RVM, Bundler, GitLab CI, icoutils
Source: https://gitlab.com/antora/antora.org
/* Contact: hello [at] opendevise.com */
/* Twitter: @opendevise */
